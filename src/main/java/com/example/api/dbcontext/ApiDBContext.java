package com.example.api.dbcontext;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ApiDBContext {
    public static Connection connection;

    static {
        try {
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/apiDB", "postgres", "ruslan000");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
