package com.example.api.models;

import org.springframework.stereotype.Component;

@Component
public class UsersDetails {
    private String email;
    private String password;

    public UsersDetails(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public UsersDetails() {

    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
