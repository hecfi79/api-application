package com.example.api.controllers;

import com.example.api.models.UsersDetails;
import com.example.api.services.UsersDetailsDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;

@RestController
@RequestMapping("/api/register")
public class RegisterController {
    private final UsersDetailsDAO usersDetailsDAO;

    @Autowired
    public RegisterController(UsersDetailsDAO usersDetailsDAO) {
        this.usersDetailsDAO = usersDetailsDAO;
    }

    @PostMapping
    public void register(@RequestBody UsersDetails usersDetails) throws SQLException {
        usersDetailsDAO.addUserDetails(usersDetails);
    }
}
